#!/usr/bin/fish
# projectctl
#
# Author: Daniel J. R. May
# Version: 0.1, 22 November 2020
#
# This is the main executable script for projectctl.


# Exit status

# The possible exit status values for the projectctl script.
set exit_status_ok 0
set exit_status_syntax_error 1
set exit_status_illegal_value_error 2
set exit_status_config_error 3


# Logging

# The possible values for $log_level.
set log_level_debug 0
set log_level_verbose 1
set log_level_info 2
set log_level_warning 3
set log_level_error 4

# Set the default value for $log_level.
set log_level $log_level_debug

# The $log_level_custom variable is set if the user customises the
# logging level via the command lie with --debug, --verbose, --quiet,
# etc.
set --erase log_level_custom 

# _log log_level messages … 
function _log
    # Validate this functions first argument (it must be a valid log level).
    switch $argv[1]
        case $log_level_debug
        case $log_level_verbose
        case $log_level_info
        case $log_level_warning
        case $log_level_error
        case '*'
            echo "[ERROR] Invalid value for log_level."
            exit $exit_status_illegal_value_error
    end

    # We only echo the message if the messages log level is
    # greater than or equal to the current log level.    
    if test $argv[1] -ge $log_level
        switch $argv[1]
            case $log_level_debug
                set_color 777777; echo "[DEBUG] $argv[2..-1]"
            case $log_level_verbose
                set_color cyan; echo "[VERBOSE] $argv[2..-1]"
            case $log_level_info
                set_color green; echo "[INFO] $argv[2..-1]"            
            case $log_level_warning
                set_color FFA500; echo "[WARNING] $argv[2..-1]"
            case $log_level_error
                set_color red; echo "[ERROR] $argv[2..-1]"
        end

        # Reset any font changes.
        set_color normal
    end    
end

# log_debug messages …
function log_debug
    # Validation of arguments is not required.
    _log $log_level_debug $argv[1..-1]
end

# log_verbose messages …
function log_verbose
    # Validation of arguments is not required.
    _log $log_level_verbose $argv[1..-1]
end

# log_info messages …
function log_info
    # Validation of arguments is not required.
    _log $log_level_info $argv[1..-1]
end

# log_warning messages …
function log_warning
    # Validation of arguments is not required.
    _log $log_level_warning $argv[1..-1]
end

# log_error messages …
function log_error
    # Validation of arguments is not required.
    _log $log_level_error $argv[1..-1]
end

# log_variable variable_name
function log_variable
    log_debug (set --show $argv[1])
end

# Configuration

# load_global_config
function load_global_config
    _set_config_base_dir
    _set_projects_config_dir
    _load_global_config_file
end

# _set_config_base_dir
function _set_config_base_dir
    # Set $config_base_dir according to if this script is named
    # 'projectctl.fish' or not. The '.fish' file extension implies
    # development mode and so we look in the current working directory
    # for the configuration.
    set script_name (status -f)
    switch $script_name
        case './projectctl.fish'
            set --global config_base_dir (realpath './config')
        case 'projectctl'
            set --global config_base_dir (realpath '/etc/projectctl')
        case '*'
            log_error 'Unexpected value of $script_name =' $script_name ' unable to configure the $config_base_dir variable.'
            exit $exit_status_config_error
    end

    # Check that $config_base_dir is a readable directory.
    if test ! -d $config_base_dir
        log_error "The expected configuration base directory $config_base_dir does not exist."
        exit $exit_status_config_error
    else if test ! -r $config_base_dir
        log_error "The configuration base directory $config_base_dir exists but is not readable."
        exit $exit_status_config_error
    else
        log_debug 'config_base_dir =' $config_base_dir        
    end
end

# _set_projects_config_dir
function _set_projects_config_dir
    # Set $projects_config_dir checking that it is a readable directory.
    set --global projects_config_dir "$config_base_dir/projects.d"
    if test ! -d $projects_config_dir
        log_error "The projects configuration directory $projects_config_dir does not exist."
        exit $exit_status_config_error
    else if test ! -r $projects_config_dir
        log_error "The projects configuration directory $projects_config_dir exists but is not readable."
        exit $exit_status_config_error
    else
        log_debug 'projects_config_dir =' $projects_config_dir        
    end
end

# _load_global_config_file
function _load_global_config_file
    # Set $gobal_config_file and check it exists and is readable.
    set global_config_file "$config_base_dir/global_config.fish"
    if test ! -f $global_config_file
        log_error "The expected global configuration file $global_config_file does not exist."
        exit $exit_status_config_error
    else if test ! -r $global_config_file
        log_error "The global configuration file $global_config_file exists but is not readable."
        exit $exit_status_config_error
    else
        log_debug 'global_config_file =' $global_config_file        
    end
    
    # Load the global configuration file.
    log_verbose "Loading $global_config_file as global configuration file…"
    source $global_config_file

    # Check there was no error loading the global configuration file.
    switch $status
        case 0
            log_debug 'local_base_dir =' $local_base_dir
        case '*'
            log_error "Failed to load $global_config_file as global configuration file, exit status was $status."
            exit $exit_status_config_error
    end
end

# _set_all_projects_config_files
function _set_all_projects_config_files
    set --global all_projects_config_files (realpath $projects_config_dir/*.fish)
    log_debug 'all_projects_config_files =' $all_projects_config_files
end

# _get_project_config_file project_name
function _set_project_config_file
    set --global project_config_file "$projects_config_dir/$argv[1].fish"
    if test ! -f $project_config_file
        log_error "The expected project configuration file $project_config_file does not exist."
        exit $exit_status_config_error
    else if test ! -r $project_config_file
        log_error "The project configuration file $project_config_file exists but is not readable."
        exit $exit_status_config_error
    else
        log_debug 'project_config_file =' $project_config_file        
    end
end

# _set_all_project_names
function _set_all_project_names
    # Configure the variables we need.
    load_global_config
    _set_all_projects_config_files

    # Clear all_project_names and then rebuild it by appending each
    # project_config_file without leading directories or '.fish'
    # extension.
    set --erase --global all_project_names
    for project_config_file in $all_projects_config_files
        set --global --append all_project_names (basename --suffix=.fish $project_config_file)
    end

    log_debug 'all_project_names =' $all_project_names
end    

# load_project_config_file project_name
function load_project_config_file
    # TODO validate arguments must have 1 argument
    _set_project_config_file $argv[1]

    # Load the project configuration file
    log_verbose "Loading $project_config_file as project configuration file…"
    source $project_config_file

    # Check there was no error loading the global configuration file
    # and log the variables which should have been defined by the
    # project configuration file.
    switch $status
        case 0
            log_debug 'local_category_dir =' $local_category_dir
            log_debug 'vcs =' $vcs
            log_debug 'name =' $name
            log_debug 'url = ' $url
        case '*'            
            log_error "Failed to load $project_config_file as project configuration file."
            exit $exit_status_config_error
    end

    # Set variables which will be required later
    set --global local_parent_dir $local_base_dir/$local_category_dir
    log_debug 'local_parent_dir =' $local_parent_dir
    
    set --global local_dir $local_parent_dir/$name
    log_debug 'local_dir =' $local_dir

    set --global current_project_vcs $vcs
    set --global current_project_url $url
end

# TODO Move default command implementations to global config or lib
# Executing subcommands

# exec_clone [project_name …]
function exec_clone
    # If this fucntion is called with zero arguments then we clone
    # every project we can find configuration files for, otherwise
    # each argument is treated as a project_name and we clone only the
    # corresponding projects.
    set --erase project_names
    switch (count $argv)
        case 0
            log_debug "Clone sub-command executed with zero arguments, so cloning all projects…"
            _set_all_project_names
            set project_names $all_project_names
        case '*'
            log_debug "Clone sub-command executed with one or more arguments, so cloning named projects…"
            set project_names $argv[1..-1]
    end

    log_debug "Projects to be cloned: $project_names"
    load_global_config
    
    for project_name in $project_names
        log_verbose "Cloning $project_name…"
        load_project_config_file $project_name
        
        if test -d $local_dir
            log_warning "Directory $local_dir already exists, skipping cloning of $url…"
            continue
        end        

        log_debug "Creating directory $local_parent_dir if it does not already exist…"
        mkdir --parents $local_parent_dir
        # TODO Error handling

        log_debug "Changing directory to $local_parent_dir…"
        cd $local_parent_dir
        # TODO Error handling

        switch $current_project_vcs
            case 'git'
                log_debug "Cloning $current_project_url with git…"
                git clone $current_project_url
                # TODO Error handling
            case '*'
                log_error "Unexpected version control system '$current_project_vcs' defined in project configuration file $p"
                exit $exit_status_config_error
        end        
    end        
end

function exec_help
    # Validate the arguments.
    if test (count $argv) -ne 0
        log_error "Help sub-command does not accept any arguments."
        return $exit_status_syntax_error
    end
    
    echo "Usage:"
    
    set_color --bold; echo -n "projectctl"
    set_color normal; echo -n " ["
    set_color --bold; echo -n "--debug"
    set_color normal; echo -n "|"
    set_color --bold; echo -n "--verbose"
    set_color normal; echo -n "|"
    set_color --bold; echo -n "--quiet"
    set_color normal; echo -n "] "
    set_color --bold; echo "help"

    set_color --bold; echo -n "projectctl"
    set_color normal; echo -n " ["
    set_color --bold; echo -n "--debug"
    set_color normal; echo -n "|"
    set_color --bold; echo -n "--verbose"
    set_color normal; echo -n "|"
    set_color --bold; echo -n "--quiet"
    set_color normal; echo -n "] "
    set_color --bold; echo -n "clone"
    set_color normal; echo -n " "
    set_color --underline; echo -n "project"
    set_color normal; echo " …"
    
    set_color normal

    return $exit_status_ok
end

# exec_list
function exec_list
    # Validate the arguments.
    if test (count $argv) -ne 0
        log_error "List sub-command does not accept any arguments, was called with $argv."
        return $exit_status_syntax_error
    end

    # Configure the variables we need.
    _set_all_project_names

    for project_name in $all_project_names
        echo $project_name
    end
end

# Testing
function test_logging
    log_debug "Debugging message."
    log_verbose "Verbose message."
    log_info "Information message."
    log_warning "Warning message."
    log_error "Error message."
end


# Command Line Argument Processing

# Log the script startup.
log_debug "Starting up projectctl…"

# Parse global options and subcommands.
set arg_index 1
for arg in $argv
    switch $arg
        case "--debug"
            if set -q custom_log_level
                log_error "Logging level already set by previous option."
                exit 1
            else
                log_debug "Setting logging level to debug…"
                set custom_log_level $log_level_debug
                set log_level $log_level_debug
            end
        case "--quiet"
            if set -q custom_log_level
                log_error "Logging level already set by previous option."
                exit 1
            else
                log_debug "Setting logging level to warning…"
                set custom_log_level $log_level_warning
                set log_level $log_level_warning
            end                        
        case "--verbose"
            if set -q custom_log_level
                log_error "Logging level already set by previous option."
                exit 1
            else
                log_debug "Setting logging level to verbose…"
                set custom_log_level $log_level_verbose
                set log_level $log_level_verbose
            end            
        case "clone"
            log_debug "Executing clone sub-command…"
            exec_clone $argv[(math $arg_index + 1)..-1]
            set exit_status $status
            break
        case "help"
            log_debug "Executing help sub-command…"
            exec_help $argv[(math $arg_index + 1)..-1]
            set exit_status $status
            break
        case "list"
            log_debug "Executing list sub-command…"
            exec_list $argv[(math $arg_index + 1)..-1]
            set exit_status $status
            break
        case '*'
            log_error "Unknown option or command."
            exit $exit_status_syntax_error
    end

    set arg_index (math $arg_index + 1)
end

# Exit the script logging the exit status.
log_debug "Exit status = $exit_status"
log_debug "Finishing up projectctl…"
exit $exit_status

